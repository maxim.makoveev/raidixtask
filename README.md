# RaidixTask


## Getting started

Install packages needed for build:
```
sudo dnf install gcc rpm-build rpm-devel rpmlint make bash coreutils rpmdevtools
```

## Complete set of commands used in bash with source code of corresponding files:

```
vim program.c

#include <stdio.h>

int main(void) {
    printf("'Test task in RAIDIX!'\n");
    return 0;
}

vim Makefile

program:
	gcc -g -o program program.c

clean:
	rm program

install:
	mkdir -p $(DESTDIR)/usr/bin
	install -m 0755 program $(DESTDIR)/usr/bin/program

cd /tmp/
vim LICENSE_GPLv3

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

mkdir /tmp/program-1.0
mv ~/program.c /tmp/program-1.0/
mv ~/Makefile /tmp/program-1.0/
cp /tmp/LICENSE_GPLv3 /tmp/program-1.0/
cd /tmp/
tar --create --file program-1.0.tar.gz program-1.0
rpmdev-setuptree
mv /tmp/program-1.0.tar.gz ~/rpmbuild/SOURCES/
cd ~/rpmbuild/SPECS
rpmdev-newspec program
vim program.spec

Name:           program
Version:	1.0 
Release:        1%{?dist}
Summary:	Simple program printing 'Test task in RAIDIX!' in STDOUT        

License:        GPLv3+
URL:            https://www.example.com/%{name}
Source0:        https://www.example.com/%{name}/releases/%{name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  make     

%description
Test task for RAIDIX company

%prep
%setup -q


%build
make %{?_smp_mflags}


%install
%make_install


%files
%license LICENSE_GPLv3
%{_bindir}/%{name}

%changelog
* Mon Sep 27 2021 Max Makoveev <maxim.makoveev@gmail.com> - 1.0-1
- initial RPM package building

rpmbuild -bs program.spec
rpmbuild -bb prolgram.spec
tree ~/rpmbuild
```

## Install | Remove RPM package:
```
sudo dnf install ~/rpmbuild/RPMS/x86_64/program-1.0-1.el8.x86_64.rpm
sudo dnf remove prolgram
```

## License
GPL3
