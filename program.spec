Name:           program
Version:	1.0 
Release:        1%{?dist}
Summary:	Simple program printing 'Test task in RAIDIX!' in STDOUT        

License:        GPLv3+
URL:            https://www.example.com/%{name}
Source0:        https://www.example.com/%{name}/releases/%{name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  make     

%description
Test task for RAIDIX company

%prep
%setup -q


%build
make %{?_smp_mflags}


%install
%make_install


%files
%license LICENSE_GPLv3
%{_bindir}/%{name}



%changelog
* Mon Sep 27 2021 Max Makoveev <maxim.makoveev@gmail.com> - 1.0-1
- initial RPM package building 
