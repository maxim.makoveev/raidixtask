program:
	gcc -g -o program program.c

clean:
	rm program

install:
	mkdir -p $(DESTDIR)/usr/bin
	install -m 0755 program $(DESTDIR)/usr/bin/program
